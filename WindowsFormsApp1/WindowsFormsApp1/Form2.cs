﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TrafficApplication
{
    public partial class Form2 : Form
    {
        private string direction;
        public string Direction {
        get {
                return direction;
            }
            set
            {
                direction = value;
            }
        }
        public Form2(string sender)
        {
            InitializeComponent();
            if (sender == "pictureBox_ID_0" || sender == "pictureBox_ID_1")
            {
                UP_button.Visible = false;
                LEFT_button.Visible = true;
                RIGHT_button.Visible = true;
                DOWN_button.Visible = true;
            }
            else if (sender == "pictureBox_ID_2")
            {
                UP_button.Visible = true;
                LEFT_button.Visible = false;
                RIGHT_button.Visible = true;
                DOWN_button.Visible = true;
            }
            else if (sender == "pictureBox_ID_5")
            {
                UP_button.Visible = true;
                LEFT_button.Visible = true;
                RIGHT_button.Visible = false;
                DOWN_button.Visible = true;
            }
            else if (sender == "pictureBox_ID_6" || sender == "pictureBox_ID_7")
            {
                UP_button.Visible = true;
                LEFT_button.Visible = true;
                RIGHT_button.Visible = true;
                DOWN_button.Visible = false;
            }
            else
            {
                UP_button.Visible = true;
                LEFT_button.Visible = true;
                RIGHT_button.Visible = true;
                DOWN_button.Visible = true;
            }

        }

        private void OK_button_Click(object sender, EventArgs e)
        {
            if (UP_button.Checked)
            {
                this.Direction = "up";
            }
            else if (DOWN_button.Checked)
            {
                this.Direction = "down";
            }
            else if (LEFT_button.Checked)
            {
                this.Direction = "left";
            }
            else if (RIGHT_button.Checked)
            {
                this.Direction = "right";
            }
            this.Close();
        }

        private void RESET_button_Click(object sender, EventArgs e)
        {
            this.Direction = null;
            this.Close();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
        }
        
    }
}
