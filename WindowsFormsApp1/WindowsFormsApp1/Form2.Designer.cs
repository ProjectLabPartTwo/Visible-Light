﻿namespace TrafficApplication
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.RIGHT_button = new System.Windows.Forms.RadioButton();
            this.LEFT_button = new System.Windows.Forms.RadioButton();
            this.DOWN_button = new System.Windows.Forms.RadioButton();
            this.UP_button = new System.Windows.Forms.RadioButton();
            this.OK_button = new System.Windows.Forms.Button();
            this.RESET_button = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.AutoSize = true;
            this.groupBox1.Controls.Add(this.RIGHT_button);
            this.groupBox1.Controls.Add(this.LEFT_button);
            this.groupBox1.Controls.Add(this.DOWN_button);
            this.groupBox1.Controls.Add(this.UP_button);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(91, 146);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // RIGHT_button
            // 
            this.RIGHT_button.AutoSize = true;
            this.RIGHT_button.Location = new System.Drawing.Point(6, 104);
            this.RIGHT_button.Name = "RIGHT_button";
            this.RIGHT_button.Size = new System.Drawing.Size(72, 21);
            this.RIGHT_button.TabIndex = 3;
            this.RIGHT_button.TabStop = true;
            this.RIGHT_button.Text = "RIGHT";
            this.RIGHT_button.UseVisualStyleBackColor = true;
            // 
            // LEFT_button
            // 
            this.LEFT_button.AutoSize = true;
            this.LEFT_button.Location = new System.Drawing.Point(6, 76);
            this.LEFT_button.Name = "LEFT_button";
            this.LEFT_button.Size = new System.Drawing.Size(63, 21);
            this.LEFT_button.TabIndex = 2;
            this.LEFT_button.TabStop = true;
            this.LEFT_button.Text = "LEFT";
            this.LEFT_button.UseVisualStyleBackColor = true;
            // 
            // DOWN_button
            // 
            this.DOWN_button.AutoSize = true;
            this.DOWN_button.Location = new System.Drawing.Point(6, 49);
            this.DOWN_button.Name = "DOWN_button";
            this.DOWN_button.Size = new System.Drawing.Size(73, 21);
            this.DOWN_button.TabIndex = 1;
            this.DOWN_button.TabStop = true;
            this.DOWN_button.Text = "DOWN";
            this.DOWN_button.UseVisualStyleBackColor = true;
            // 
            // UP_button
            // 
            this.UP_button.AutoSize = true;
            this.UP_button.Location = new System.Drawing.Point(6, 21);
            this.UP_button.Name = "UP_button";
            this.UP_button.Size = new System.Drawing.Size(48, 21);
            this.UP_button.TabIndex = 0;
            this.UP_button.TabStop = true;
            this.UP_button.Text = "UP";
            this.UP_button.UseVisualStyleBackColor = true;
            // 
            // OK_button
            // 
            this.OK_button.Location = new System.Drawing.Point(112, 59);
            this.OK_button.Name = "OK_button";
            this.OK_button.Size = new System.Drawing.Size(62, 23);
            this.OK_button.TabIndex = 1;
            this.OK_button.Text = "OK";
            this.OK_button.UseVisualStyleBackColor = true;
            this.OK_button.Click += new System.EventHandler(this.OK_button_Click);
            // 
            // RESET_button
            // 
            this.RESET_button.Location = new System.Drawing.Point(112, 88);
            this.RESET_button.Name = "RESET_button";
            this.RESET_button.Size = new System.Drawing.Size(62, 23);
            this.RESET_button.TabIndex = 2;
            this.RESET_button.Text = "RESET";
            this.RESET_button.UseVisualStyleBackColor = true;
            this.RESET_button.Click += new System.EventHandler(this.RESET_button_Click);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(186, 165);
            this.Controls.Add(this.RESET_button);
            this.Controls.Add(this.OK_button);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form2";
            this.Text = "Form2";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton RIGHT_button;
        private System.Windows.Forms.RadioButton LEFT_button;
        private System.Windows.Forms.RadioButton DOWN_button;
        private System.Windows.Forms.RadioButton UP_button;
        private System.Windows.Forms.Button OK_button;
        private System.Windows.Forms.Button RESET_button;
    }
}