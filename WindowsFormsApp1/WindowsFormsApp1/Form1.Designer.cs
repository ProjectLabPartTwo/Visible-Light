﻿namespace TrafficApplication
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.bestandToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.versturenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.configuratieToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resettenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.afsluitenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pictureBox_ID_7 = new System.Windows.Forms.PictureBox();
            this.pictureBox_ID_6 = new System.Windows.Forms.PictureBox();
            this.pictureBox_ID_5 = new System.Windows.Forms.PictureBox();
            this.pictureBox_ID_1 = new System.Windows.Forms.PictureBox();
            this.pictureBox_ID_4 = new System.Windows.Forms.PictureBox();
            this.pictureBox_ID_0 = new System.Windows.Forms.PictureBox();
            this.pictureBox_ID_3 = new System.Windows.Forms.PictureBox();
            this.pictureBox_ID_2 = new System.Windows.Forms.PictureBox();
            this.Background_streetplan = new System.Windows.Forms.PictureBox();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_ID_7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_ID_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_ID_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_ID_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_ID_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_ID_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_ID_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_ID_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Background_streetplan)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bestandToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1344, 28);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // bestandToolStripMenuItem
            // 
            this.bestandToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.versturenToolStripMenuItem,
            this.configuratieToolStripMenuItem,
            this.resettenToolStripMenuItem,
            this.afsluitenToolStripMenuItem});
            this.bestandToolStripMenuItem.Name = "bestandToolStripMenuItem";
            this.bestandToolStripMenuItem.Size = new System.Drawing.Size(74, 24);
            this.bestandToolStripMenuItem.Text = "Bestand";
            // 
            // versturenToolStripMenuItem
            // 
            this.versturenToolStripMenuItem.Name = "versturenToolStripMenuItem";
            this.versturenToolStripMenuItem.Size = new System.Drawing.Size(216, 26);
            this.versturenToolStripMenuItem.Text = "Versturen";
            // 
            // configuratieToolStripMenuItem
            // 
            this.configuratieToolStripMenuItem.Name = "configuratieToolStripMenuItem";
            this.configuratieToolStripMenuItem.Size = new System.Drawing.Size(216, 26);
            this.configuratieToolStripMenuItem.Text = "Configuratie";
            // 
            // resettenToolStripMenuItem
            // 
            this.resettenToolStripMenuItem.Name = "resettenToolStripMenuItem";
            this.resettenToolStripMenuItem.Size = new System.Drawing.Size(216, 26);
            this.resettenToolStripMenuItem.Text = "Resetten";
            this.resettenToolStripMenuItem.Click += new System.EventHandler(this.resettenToolStripMenuItem_Click);
            // 
            // afsluitenToolStripMenuItem
            // 
            this.afsluitenToolStripMenuItem.Name = "afsluitenToolStripMenuItem";
            this.afsluitenToolStripMenuItem.Size = new System.Drawing.Size(216, 26);
            this.afsluitenToolStripMenuItem.Text = "Afsluiten";
            this.afsluitenToolStripMenuItem.Click += new System.EventHandler(this.afsluitenToolStripMenuItem_Click);
            // 
            // pictureBox_ID_7
            // 
            this.pictureBox_ID_7.BackColor = System.Drawing.Color.White;
            this.pictureBox_ID_7.Image = global::TrafficApplication.Properties.Resources.circle;
            this.pictureBox_ID_7.Location = new System.Drawing.Point(886, 621);
            this.pictureBox_ID_7.Name = "pictureBox_ID_7";
            this.pictureBox_ID_7.Size = new System.Drawing.Size(60, 60);
            this.pictureBox_ID_7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_ID_7.TabIndex = 10;
            this.pictureBox_ID_7.TabStop = false;
            // 
            // pictureBox_ID_6
            // 
            this.pictureBox_ID_6.BackColor = System.Drawing.Color.White;
            this.pictureBox_ID_6.Image = global::TrafficApplication.Properties.Resources.circle;
            this.pictureBox_ID_6.Location = new System.Drawing.Point(365, 621);
            this.pictureBox_ID_6.Name = "pictureBox_ID_6";
            this.pictureBox_ID_6.Size = new System.Drawing.Size(60, 60);
            this.pictureBox_ID_6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_ID_6.TabIndex = 9;
            this.pictureBox_ID_6.TabStop = false;
            // 
            // pictureBox_ID_5
            // 
            this.pictureBox_ID_5.BackColor = System.Drawing.Color.White;
            this.pictureBox_ID_5.Image = global::TrafficApplication.Properties.Resources.circle;
            this.pictureBox_ID_5.Location = new System.Drawing.Point(1272, 331);
            this.pictureBox_ID_5.Name = "pictureBox_ID_5";
            this.pictureBox_ID_5.Size = new System.Drawing.Size(60, 60);
            this.pictureBox_ID_5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_ID_5.TabIndex = 8;
            this.pictureBox_ID_5.TabStop = false;
            // 
            // pictureBox_ID_1
            // 
            this.pictureBox_ID_1.BackColor = System.Drawing.Color.White;
            this.pictureBox_ID_1.Image = global::TrafficApplication.Properties.Resources.circle;
            this.pictureBox_ID_1.Location = new System.Drawing.Point(886, 40);
            this.pictureBox_ID_1.Name = "pictureBox_ID_1";
            this.pictureBox_ID_1.Size = new System.Drawing.Size(60, 60);
            this.pictureBox_ID_1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_ID_1.TabIndex = 7;
            this.pictureBox_ID_1.TabStop = false;
            // 
            // pictureBox_ID_4
            // 
            this.pictureBox_ID_4.BackColor = System.Drawing.Color.White;
            this.pictureBox_ID_4.Image = global::TrafficApplication.Properties.Resources.circle;
            this.pictureBox_ID_4.Location = new System.Drawing.Point(886, 331);
            this.pictureBox_ID_4.Name = "pictureBox_ID_4";
            this.pictureBox_ID_4.Size = new System.Drawing.Size(60, 60);
            this.pictureBox_ID_4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_ID_4.TabIndex = 6;
            this.pictureBox_ID_4.TabStop = false;
            // 
            // pictureBox_ID_0
            // 
            this.pictureBox_ID_0.BackColor = System.Drawing.Color.White;
            this.pictureBox_ID_0.Image = global::TrafficApplication.Properties.Resources.circle;
            this.pictureBox_ID_0.Location = new System.Drawing.Point(365, 40);
            this.pictureBox_ID_0.Name = "pictureBox_ID_0";
            this.pictureBox_ID_0.Size = new System.Drawing.Size(60, 60);
            this.pictureBox_ID_0.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_ID_0.TabIndex = 5;
            this.pictureBox_ID_0.TabStop = false;
            // 
            // pictureBox_ID_3
            // 
            this.pictureBox_ID_3.BackColor = System.Drawing.Color.White;
            this.pictureBox_ID_3.Image = global::TrafficApplication.Properties.Resources.circle;
            this.pictureBox_ID_3.Location = new System.Drawing.Point(365, 331);
            this.pictureBox_ID_3.Name = "pictureBox_ID_3";
            this.pictureBox_ID_3.Size = new System.Drawing.Size(60, 60);
            this.pictureBox_ID_3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_ID_3.TabIndex = 4;
            this.pictureBox_ID_3.TabStop = false;
            // 
            // pictureBox_ID_2
            // 
            this.pictureBox_ID_2.BackColor = System.Drawing.Color.White;
            this.pictureBox_ID_2.Image = global::TrafficApplication.Properties.Resources.circle;
            this.pictureBox_ID_2.Location = new System.Drawing.Point(12, 331);
            this.pictureBox_ID_2.Name = "pictureBox_ID_2";
            this.pictureBox_ID_2.Size = new System.Drawing.Size(60, 60);
            this.pictureBox_ID_2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_ID_2.TabIndex = 3;
            this.pictureBox_ID_2.TabStop = false;
            // 
            // Background_streetplan
            // 
            this.Background_streetplan.BackgroundImage = global::TrafficApplication.Properties.Resources.interfaceBackground;
            this.Background_streetplan.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Background_streetplan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Background_streetplan.Location = new System.Drawing.Point(0, 28);
            this.Background_streetplan.Name = "Background_streetplan";
            this.Background_streetplan.Size = new System.Drawing.Size(1344, 665);
            this.Background_streetplan.TabIndex = 1;
            this.Background_streetplan.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1344, 693);
            this.Controls.Add(this.pictureBox_ID_7);
            this.Controls.Add(this.pictureBox_ID_6);
            this.Controls.Add(this.pictureBox_ID_5);
            this.Controls.Add(this.pictureBox_ID_1);
            this.Controls.Add(this.pictureBox_ID_4);
            this.Controls.Add(this.pictureBox_ID_0);
            this.Controls.Add(this.pictureBox_ID_3);
            this.Controls.Add(this.pictureBox_ID_2);
            this.Controls.Add(this.Background_streetplan);
            this.Controls.Add(this.menuStrip1);
            this.DoubleBuffered = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_ID_7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_ID_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_ID_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_ID_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_ID_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_ID_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_ID_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_ID_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Background_streetplan)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem bestandToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem versturenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem configuratieToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem resettenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem afsluitenToolStripMenuItem;
        private System.Windows.Forms.PictureBox Background_streetplan;
        private System.Windows.Forms.PictureBox pictureBox_ID_2;
        private System.Windows.Forms.PictureBox pictureBox_ID_3;
        private System.Windows.Forms.PictureBox pictureBox_ID_0;
        private System.Windows.Forms.PictureBox pictureBox_ID_4;
        private System.Windows.Forms.PictureBox pictureBox_ID_1;
        private System.Windows.Forms.PictureBox pictureBox_ID_5;
        private System.Windows.Forms.PictureBox pictureBox_ID_6;
        private System.Windows.Forms.PictureBox pictureBox_ID_7;
    }
}

