﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TrafficApplication
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            foreach (var box in Controls.OfType<PictureBox>())
            {
                box.Click += pictureBox_Click;
            }
        }

        private void afsluitenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are You Sure To Exit Program?", "Exit", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                Application.Exit();
            }
        }


        private void pictureBox_Click(object sender, EventArgs e)
        {
            if (((PictureBox)sender).Name != "Background_streetplan")
            {
                Form2 f = new Form2(((PictureBox)sender).Name);
                f.ShowDialog();
                string direction = f.Direction;
                switch (direction)
                {
                    case "up": ((PictureBox)sender).Image = Properties.Resources.circle_arrow_up; break;
                    case "down": ((PictureBox)sender).Image = Properties.Resources.circle_arrow_down; break;
                    case "left": ((PictureBox)sender).Image = Properties.Resources.circle_arrow_left; break;
                    case "right": ((PictureBox)sender).Image = Properties.Resources.circle_arrow_right; break;
                    default: ((PictureBox)sender).Image = Properties.Resources.circle; break;
                }
            }
        }

        private void resettenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (var box in Controls.OfType<PictureBox>())
            {
                if (box.Name != "Background_streetplan")
                {
                    box.Image = Properties.Resources.circle;
                }
            }
        }
    }
}
