﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TrafficApplication
{
    public partial class Traffic_form : Form
    {
        Image[] pictures = new Image[] { null,
                                         Properties.Resources.move_forward,
                                         Properties.Resources.turn_right,
                                         Properties.Resources.turn_180,
                                         Properties.Resources.turn_left};
        Image check = Properties.Resources.circle;
        String[] direction = new String[] { "ignore", "up", "right", "down", "left"};

        String[] boxes = new String[8];
        int[] indicators = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
        Communication comm = new Communication();
        WaitForm loader = new WaitForm();

        //Default values for communication, can be modified in the configform
        String ip = "192.168.0.1";
        int port = 4000;
        
        public Traffic_form()
        {
            InitializeComponent();
            int i = 0;
            foreach (var box in Controls.OfType<PictureBox>())
            {
                if (box.Name != "Background_streetplan")
                {
                    box.MouseDown += pictureBox_Click;
                    boxes[i] = box.Name;
                    i++;
                }
                    
            }
            this.ControlBox = false;
            FormBorderStyle = FormBorderStyle.FixedSingle;
        }

        private void afsluitenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are You Sure To Exit Program?", "Exit", MessageBoxButtons.OKCancel,MessageBoxIcon.Exclamation) == DialogResult.OK)
            {
                Application.Exit();
            }
        }

        //When clicked on a picturebox, changes the current direction of that picture.
        //If the next direction would be invalid for the data, changes the direction two times.
        private void pictureBox_Click(object sender, EventArgs e)
        {
            String temp = ((PictureBox)sender).Name;
            int i = 0;
            for (; i < 8; i++)
            {
                if (temp == boxes[i]) { indicators[i] = (indicators[i] + 1) % 5; break; }
            }

            /*switch (((PictureBox)sender).Name)
            {
                case "pictureBox_ID_0":
                case "pictureBox_ID_1": if (indicators[i] == 1) { indicators[i]++; } break;
                case "pictureBox_ID_2": if (indicators[i] == 4) { indicators[i] = 0; } break;
                case "pictureBox_ID_5": if (indicators[i] == 2) { indicators[i]++; } break;
                case "pictureBox_ID_6":
                case "pictureBox_ID_7": if (indicators[i] == 3) { indicators[i]++; } break;
            }*/

            ((PictureBox)sender).Image = pictures[indicators[i]];
        }

        //Puts all the pictureboxes to the 'no direction'
        private void resettenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (var box in Controls.OfType<PictureBox>())
            {
                if (box.Name != "Background_streetplan")
                {
                    box.Image = pictures[0];
                }
            }
            for(int i = 0; i<8; i++)
            {
                indicators[i] = 0;
            }
        }

        //Starts the loading form with info for the user and starts thread for connecting and sending the data.
        private void versturenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Do you want to send program?", "Send", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                loader.Show();
                loader.setLabelComm(ip, port);
                loader.Refresh();
                Thread init = new Thread(sendThread);
                init.Start();
            }
        }

        //Thread which connects and sends data. If an exception is caught, show a messagebox.
        public void sendThread()
        {
            try
            {
                comm.FormDataReset();
                foreach (var box in Controls.OfType<PictureBox>())
                {
                    if (box.Name != "Background_streetplan")
                    {
                        String temp = box.Name;
                        int i = 0;
                        for (; i < 8; i++)
                        {
                            if (temp == boxes[i]) { break; }
                        }
                        comm.FormData(box.Name, direction[indicators[i]]);
                    }
                }
                comm.ClientSend(ip, port);
            }
            catch (System.Net.Sockets.SocketException)
            {
                MessageBox.Show("Connection could not be instanced, server not found.", "Connection Failure", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            hideWaitForm();
        }

        //Used to hide the Waitform when using threads.
        public void hideWaitForm()
        {
            loader.Invoke(new CloseForm(loader.Hide));
        }

        //Configures the connection.
        private void configuratieToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Config_form f = new Config_form(ip, port);
            f.ShowDialog();
            ip = f.Ip;
            port = f.Port;
        }
    }
}
