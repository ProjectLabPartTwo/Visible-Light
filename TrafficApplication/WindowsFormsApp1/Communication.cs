﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;

namespace TrafficApplication
{
    class Communication
    {
        TcpClient client = new TcpClient();
        String data = "";
        public Communication()
        {
            
        }

        //Initializes the connection with the given Ipaddress on the given port.
        //If initialization isn't possible, return an exception.
        public string ClientSend(String IpAddress, int port)
        {
            try
            {
                TcpClient client = new TcpClient();
                client.Connect(IpAddress, port);

                //Sends the data to the initialized server. If the data transfer fails an error is given.
                try
                {
                    NetworkStream stream = client.GetStream();
                    byte[] outputStream = Encoding.ASCII.GetBytes(data);
                    stream.Write(outputStream, 0, outputStream.Length);
                    stream.Flush();

                    byte[] inputStream = new byte[65536];
                    stream.Read(inputStream, 0, (int)client.ReceiveBufferSize);
                    string returnData = Encoding.ASCII.GetString(inputStream);
                    return returnData;

                }
                catch (Exception)
                {
                    throw (new Exception());
                }
            }
            catch(SocketException exc)
            {
                throw(new SocketException());
            }
        }

        //Concatenates the given box id and the direction to the current data
        public void FormData(String box_id, String box_direction)
        {
            data = data + box_id.Substring(14) + "-" + box_direction + " ";
        }

        //Clears the current data.
        public void FormDataReset()
        {
            data = "";
        }

    }
}
