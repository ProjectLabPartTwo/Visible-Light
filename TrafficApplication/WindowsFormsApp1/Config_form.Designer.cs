﻿namespace TrafficApplication
{
    partial class Config_form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.IP_BOX = new System.Windows.Forms.TextBox();
            this.PORT_BOX = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.OK_BUTTON = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // IP_BOX
            // 
            this.IP_BOX.Location = new System.Drawing.Point(73, 5);
            this.IP_BOX.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.IP_BOX.Name = "IP_BOX";
            this.IP_BOX.Size = new System.Drawing.Size(91, 20);
            this.IP_BOX.TabIndex = 0;
            // 
            // PORT_BOX
            // 
            this.PORT_BOX.Location = new System.Drawing.Point(200, 5);
            this.PORT_BOX.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.PORT_BOX.Name = "PORT_BOX";
            this.PORT_BOX.Size = new System.Drawing.Size(38, 20);
            this.PORT_BOX.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 7);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Ip Address:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(167, 7);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Port:";
            // 
            // OK_BUTTON
            // 
            this.OK_BUTTON.Location = new System.Drawing.Point(242, 5);
            this.OK_BUTTON.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.OK_BUTTON.Name = "OK_BUTTON";
            this.OK_BUTTON.Size = new System.Drawing.Size(35, 19);
            this.OK_BUTTON.TabIndex = 4;
            this.OK_BUTTON.Text = "OK";
            this.OK_BUTTON.UseVisualStyleBackColor = true;
            this.OK_BUTTON.Click += new System.EventHandler(this.OK_BUTTON_Click);
            // 
            // Config_form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 29);
            this.Controls.Add(this.OK_BUTTON);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.PORT_BOX);
            this.Controls.Add(this.IP_BOX);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Config_form";
            this.Text = "Configuration setup";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox IP_BOX;
        private System.Windows.Forms.TextBox PORT_BOX;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button OK_BUTTON;
    }
}