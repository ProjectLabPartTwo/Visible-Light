﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TrafficApplication
{
    public partial class Config_form : Form
    {
        String ip;
        int port;

        //The property of ip
        public String Ip
        {
            get
            {
                return ip;
            }
            set
            {
                ip = value;
            }
        }

        //The property of port
        public int Port
        {
            get
            {
                return port;
            }
            set
            {
                port = value;
            }
        }
        
        public Config_form(string ip, int port)
        {
            InitializeComponent();
            this.ControlBox = false;
            FormBorderStyle = FormBorderStyle.FixedSingle;
            IP_BOX.Text = ip;
            PORT_BOX.Text = Convert.ToString(port);
        }


        private void OK_BUTTON_Click(object sender, EventArgs e)
        {
            Ip = IP_BOX.Text;
            Port = Convert.ToInt16(PORT_BOX.Text);
            this.Close();
        }
    }
}
