﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TrafficApplication
{
    public delegate void CloseForm();
    public partial class WaitForm : Form
    {
        public WaitForm()
        {
            InitializeComponent();
            FormBorderStyle = FormBorderStyle.FixedSingle;
        }

        public void setLabelComm(string ip, int port)
        {
            InfoLabel.Text = "Connecting to " + ip + " on port " + port;
        }
    }
}
